use indicatif::{ProgressBar, ProgressStyle};
use reqwest::Client;
use scraper::Selector;
use std::{
    collections::HashSet,
    fs,
    path::Path,
    time::Duration,
};
use tokio::sync::mpsc;
use futures::{stream, StreamExt};

const BASE_SEARCH_PATH: &'static str = "https://1337x.to/johncena141-torrents";
const BASE_PATH: &'static str = "https://1337x.to";
pub const CACHE_PATH: &'static str = "cache.csv";

const TIMEOUT_DURATION: Duration = Duration::from_secs(4);

#[derive(Debug)]
pub struct ElementInfo {
    pub name: String,
    pub link: String,
    pub hash: String,
}

pub async fn load_new_elements(write: bool, trials: u32) -> Vec<ElementInfo> {
    let known_hashes = load_csv(CACHE_PATH);
    let new_elements = fetch_new_elements(BASE_SEARCH_PATH, known_hashes, 4, trials).await;
    if write {
        let mut writer = CacheWriter::new(CACHE_PATH, WriteMode::Append);
        for ElementInfo {name, link, hash} in new_elements.iter() {
            writer.write(name, link, hash);
        }
        writer.flush();
    }
    new_elements
}

/// The set of hashes in return vec is always disjoint from knowkn_hashes.
async fn fetch_new_elements(
    page: &str,
    known_hashes: HashSet<String>,
    max_pages: usize,
    max_requests: u32,
) -> Vec<ElementInfo> {
    let page_iterator = PageIterator::new(max_pages, String::from(page));
    let mut ret = Vec::new();
    let client = Client::builder().build().unwrap();
    for page in page_iterator {
        let parsed = parse_index_page(&client.clone(), &page, max_requests).await;

        let elements = stream::iter(parsed.into_iter()).then(|(name, link)| {
            let client = client.clone();
            async move {
                let hash = get_hash(&client, &link, max_requests).await.unwrap();
                ElementInfo {name, link, hash}
            }
        }).collect::<Vec<ElementInfo>>().await;
        
        let mut intersects = false;
        for el in elements {
            if known_hashes.contains(&el.hash) {
                intersects = true;
            } else {
                ret.push(el)
            }
            tokio::task::yield_now().await;
        }
        if intersects {
            break
        }
    }
    ret
}

pub async fn create_cache<P>(cache_file: P, num_pages: usize, trials: u32, buffer_size: usize)
where
    P: AsRef<Path>,
{
    const FLUSH_RATE: u16 = 100;
    let progress_bar = ProgressBar::new(num_pages as u64 * 20).with_style(
        ProgressStyle::with_template("[{elapsed_precise}] {bar:40.cyan/blue} {pos:>3}/{len:7}")
            .unwrap(),
    );
    progress_bar.tick();

    let mut csv_writer = CacheWriter::new(cache_file, WriteMode::Write);
    let page_iterator  = PageIterator::new(num_pages, String::from(BASE_SEARCH_PATH));
    let client         = Client::builder().timeout(TIMEOUT_DURATION)
                                          .build().unwrap();

    let (tx, mut rx) = mpsc::channel(500);

    tokio::spawn(async move {
        let mut counter: u16 = 0;
        while let Some(value) = rx.recv().await {
            let ElementInfo { name, link, hash } = value;
            csv_writer.write(&name, &link, &hash);
            progress_bar.inc(1);
            counter = (counter + 1) % FLUSH_RATE;
            if counter == 0 {
                csv_writer.flush();
            }
        }
        progress_bar.finish();
    });

    let index_pages_stream = stream::iter(page_iterator).map(|page| {
        let client = client.clone();
        async move {
            let index_page = parse_index_page(&client.clone(), &page, trials).await;
            index_page
        }
    }).buffered(2)
      .flat_map(|index_pages| stream::iter(index_pages));

    index_pages_stream.map(|index_page| {
        let client = client.clone();
        let tx     = tx.clone();
        async move {
            let name    = index_page.0;
            let link    = index_page.1;
            let hash    = get_hash(&client, &link, trials).await.unwrap();
            let element = ElementInfo {name, link, hash};
            tx.send(element).await.unwrap();
        }
    }).buffer_unordered(buffer_size).collect::<Vec<()>>().await;
}

/// Returns a vec of (name, link)
async fn parse_index_page(client: &Client, page: &str, max_requests: u32) -> Vec<(String, String)> {
    let response = multiple_get(client, page, max_requests).await;
    let document = scraper::Html::parse_document(&response);
    let table_selector = Selector::parse("table>tbody>tr>td>a").unwrap();
    let matched = document.select(&table_selector);
    let false_positive_selector = Selector::parse("i").unwrap();
    let mut ret = Vec::new();
    for element in matched {
        {
            let fp_test = element.select(&false_positive_selector);
            if fp_test.count() != 0 {
                continue;
            }
        }
        let name = element.inner_html();
        let link = {
            let path = element.value().attr("href").unwrap();
            format!("{}{}", BASE_PATH, path)
        };
        ret.push((name, link));
    }
    ret
}

/// Returns hash in given page
async fn get_hash(client: &Client, page: &str, max_requests: u32) -> Result<String, reqwest::Error> {
    let response = multiple_get(client, page, max_requests).await;
    let document = scraper::Html::parse_document(&response);
    let selector = Selector::parse(r#"div[class="infohash-box"]>p>span"#).unwrap();
    let mut matched = document.select(&selector);
    Ok(matched.next().unwrap().inner_html())
}


async fn multiple_get(client: &Client, page: &str, max_requests: u32) -> String {
    let mut iterations = 0;
    let mut timeout = TIMEOUT_DURATION;
    loop {
        let result = client.request(reqwest::Method::GET, page)
                           .timeout(timeout).send().await;
        if let Ok(response) = result {
            if let Ok(text) = response.text().await {
                return text;
            }
        }
        iterations += 1;
        timeout = 2 * timeout;
        // eprintln!("we tried {} time to retrieve page {}, but failed", iterations, page);
        if iterations >= max_requests {
            panic!("we tried {} time to retrieve page {}, but failed", iterations, page);
        }
    }
}

struct PageIterator {
    page: usize,
    max_page: usize,
    base_path: String,
}
impl Iterator for PageIterator {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        if self.page > self.max_page {
            return None;
        }
        let ret = format!("{base}/{page}/", base = self.base_path, page = self.page);
        self.page += 1;
        Some(ret)
    }

    fn count(self) -> usize
    where
        Self: Sized,
    {
        self.max_page - self.page + 1
    }
}
impl PageIterator {
    fn new(max_page: usize, base_path: String) -> Self {
        PageIterator {
            page: 1,
            max_page,
            base_path,
        }
    }
}

enum WriteMode {
    Write,
    Append,
}
struct CacheWriter {
    writer: csv::Writer<fs::File>,
}
impl CacheWriter {
    fn new<P>(path: P, mode: WriteMode) -> Self
    where
        P: AsRef<Path>,
    {
        let file_writer = {
            use WriteMode::*;
            match mode {
                Write => {
                    let _ = fs::remove_file(&path);
                    fs::OpenOptions::new()
                        .write(true)
                        .create(true)
                        .open(&path)
                        .unwrap()
                }
                Append => fs::OpenOptions::new()
                    .append(true)
                    .create(true)
                    .open(&path)
                    .unwrap(),
            }
        };
        let writer = csv::WriterBuilder::new()
            .quote_style(csv::QuoteStyle::Always)
            .from_writer(file_writer);
        CacheWriter { writer }
    }

    fn write(&mut self, name: &str, link: &str, hash: &str) {
        self.writer.write_record(&[hash, name, link]).unwrap();
    }

    fn flush(&mut self) {
        self.writer.flush().unwrap();
    }
}

fn load_csv<P>(path: P) -> HashSet<String>
where
    P: AsRef<Path>,
{
    let mut ret = HashSet::new();
    let mut reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_path(&path)
        .unwrap();
    for result in reader.records() {
        let record = result.unwrap();
        let hash = record.get(0).unwrap();
        ret.insert(String::from(hash));
    }
    ret
}

