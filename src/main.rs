extern crate clap;
extern crate teloxide;
extern crate tokio;

use clap::Parser;

mod parser;
use parser::*;

use teloxide::prelude::*;
use tokio::{
    runtime, signal,
    sync::mpsc,
    time::{self, Duration},
};
use std::{
    collections::HashSet,
    fs,
    io::{Read, Write},
    mem,
};

const MAX_PAGES: usize = 83;
const ACTIVE_USERS_PATH: &'static str = "active_users.csv";
const REBUILD_BUFFER_SIZE: usize = 100;
const MAX_REQUESTS_FOR_PAGE: u32 = 8;

#[derive(Parser, Debug)]
#[clap(author, version)]
enum Args {
    /// Removes the cache file and downloads all the data again
    Rebuild {
        /// number of index pages to parse
        #[clap(short, long)]
        pages: Option<usize>,
        /// how many times try to download a webpage
        #[clap(short, long)]
        trials: Option<u32>,
        /// how many items in concurrency buffer for downloading
        #[clap(short, long)]
        buffer: Option<usize>,
    },
    /// Prints the new data (wrt the cache)
    Load {
        ///if enabled writes to the cache the new data
        #[clap(short, long)]
        /// set true to write cache when updating
        write: bool,
        /// how many times try to download a webpage
        #[clap(short, long)]
        trials: Option<u32>,
    },
    /// Starts the telegram server
    Server {
        /// how many seconds wait between a load and another
        #[clap(short, long)]
        refresh_rate: u64, 
    },
}

fn main() {
    let args = Args::parse();
    use Args::*;
    match args {
        Rebuild { pages, trials, buffer } => rebuild(pages, trials, buffer),
        Load { write, trials } => load(write, trials),
        Server { refresh_rate } => server(refresh_rate),
    }
}

#[tokio::main]
async fn rebuild(pages: Option<usize>, trials: Option<u32>, buffer: Option<usize>) {
    let num_pages = match pages {
        Some(p) => p,
        None => MAX_PAGES,
    };
    let num_trials = match trials {
        Some(x) => x,
        None => MAX_REQUESTS_FOR_PAGE,
    };
    let num_buffer = match buffer {
        Some(x) => x,
        None => REBUILD_BUFFER_SIZE,
    };
    create_cache(CACHE_PATH, num_pages, num_trials, num_buffer).await;
}

#[tokio::main]
async fn load(write: bool, trials: Option<u32>) {
    let num_trials = match trials {
        Some(x) => x,
        None => MAX_REQUESTS_FOR_PAGE,
    };
    let new_elem = load_new_elements(write, num_trials).await;
    for el in new_elem {
        println!("{}  {}", el.hash, el.name);
    }
}

fn server(refresh_rate: u64) {
    let runtime = runtime::Builder::new_multi_thread()
        .thread_name("server_thread")
        .enable_time()
        .enable_io()
        .build()
        .unwrap();
    let _guard = runtime.enter();

    runtime.block_on(async {
        let (end_signal_tx, mut end_signal_rx) = mpsc::channel::<u8>(1);
        let (sub_tx, mut sub_rx) = mpsc::channel(20);

        let sender_c = end_signal_tx.clone();
        let respond_handler = tokio::spawn(async move {
            let _ = sender_c;
            let bot = Bot::from_env().auto_send(); // need to set TELOXIDE_TOKEN
            teloxide::repl(bot, move |message: Message, bot: AutoSend<Bot>| {
                let sub_tx = sub_tx.clone();
                async move {
                    let chat_id = message.chat.id;
                    println!("{:#?}", message.text());
                    bot.send_message(chat_id, "Bella a tutti\n~Marche").await.unwrap();
                    sub_tx.send(message.chat.id).await.unwrap();
                    bot.send_message(chat_id, "Ora sei iscritto").await.unwrap();
                    respond(())
                }
            })
            .await;
        });

        let mut chats = {
            let mut chats = HashSet::new();
            let file_res = fs::OpenOptions::new()
                                           .read(true)
                                           .open(ACTIVE_USERS_PATH);
            if let Ok(mut file) = file_res {
                let text = {
                    let mut text = String::new();
                    file.read_to_string(&mut text).unwrap();
                    text
                };
                for line in text.split('\n') {
                    let line = line.trim();
                    let id: i64 = if let Ok(id) = line.parse() {
                        id
                    } else {
                        continue
                    };
                    let chat = ChatId(id);
                    chats.insert(chat);
                }
            } 
            chats
        };

        let sender_c = end_signal_tx.clone();
        let some_handler = tokio::spawn(async move {
            let _ = sender_c;
            let bot = Bot::from_env().auto_send();
            let mut new_chats = Vec::new();
            loop {
                while let Ok(id) = sub_rx.try_recv() {
                    if !chats.contains(&id) {
                        println!("NEW USER!\n{:#?}", &id);
                        new_chats.push(id);
                    }
                }
                if !new_chats.is_empty() {
                    let mut file = fs::OpenOptions::new()
                                               .append(true)
                                               .create(true)
                                               .open(ACTIVE_USERS_PATH)
                                               .unwrap();
                    for chat in &new_chats {
                        let chat_str = format!("{}\n", &chat.0.to_string());
                        file.write(chat_str.as_bytes()).unwrap();
                    }
                    file.flush().unwrap();
                    chats.reserve(new_chats.len());
                    for chat in mem::take(&mut new_chats).into_iter() {
                        chats.insert(chat);
                    }
                }
                println!("Updating from jc...");
                update_from_jc(&bot, &chats).await;
                println!("Update done");
                tokio::select! {
                    _ = time::sleep(Duration::from_secs(refresh_rate)) => {},
                    Some(id) = sub_rx.recv() => {
                        if !chats.contains(&id) {
                            println!("NEW USER!\n{:#?}", &id);
                            new_chats.push(id);
                        }
                    },
                }
            }
        });

        drop(end_signal_tx);
        tokio::select! {
            _ = signal::ctrl_c() => {},
            _ = end_signal_rx.recv() => {},
        }

        respond_handler.abort();
        some_handler.abort();
    });
}

async fn update_from_jc(bot: &AutoSend<Bot>, chats: &HashSet<ChatId>) {
    // @todo parametrize this
    let new_element = load_new_elements(true, MAX_REQUESTS_FOR_PAGE).await;
    for el in new_element {
        let name = el.name;
        for id in chats.iter() {
            bot.send_message(*id, &name).await.unwrap();
        }
    }
}
